<?php

declare(strict_types = 1);

namespace App\Services\Converter;

use Exception;
use Illuminate\Support\Arr;
use App\Services\Converter\Formats\CsvFormatHandler;
use App\Services\Converter\Formats\FormatHandlerInterface;
use App\Services\Converter\Formats\JsonFormatHandler;
use App\Services\Converter\Formats\XmlFormatHandler;

use function array_keys;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

/**
 * Class ConverterHandler
 *
 * @package App\Services\Converter
 */
class ConverterHandler
{
    /**
     * @var array
     */
    private $handlersPool = [
        CsvFormatHandler::class,
        XmlFormatHandler::class,
        JsonFormatHandler::class,
    ];

    /**
     * @var array
     */
    private $handlersMap = [];

    /**
     * ConverterHandler constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setFormatHandlers();
    }

    /**
     * @param string $format
     *
     * @return null|\App\Services\Converter\Formats\FormatHandlerInterface
     */
    public function getFormatHandler(string $format): ?FormatHandlerInterface
    {
        return Arr::get($this->handlersMap, $format);
    }

    /**
     * @return array
     */
    public function getAvailableFormats(): array
    {
        return array_keys($this->handlersMap);
    }

    /**
     * @param string $format
     *
     * @return null|string
     */
    public function getMimeType(string $format): ?string
    {
        $map = [
            'csv' => 'text/csv',
            'json' => 'application/json',
            'xml' => 'application/xml',
        ];

        return Arr::get($map, $format);
    }

    /**
     * @param string $publicPath
     *
     * @return string
     */
    public function getGeneratedFileRealPath(string $publicPath): string
    {
        return Config::get('filesystems.disks.public.root').Str::after($publicPath, '/storage');
    }

    /**
     * @throws \Exception
     *
     * @return void
     */
    private function setFormatHandlers(): void
    {
        foreach ($this->handlersPool as $handlerClass) {
            $handlerInstance = new $handlerClass();

            if (!$handlerInstance instanceof FormatHandlerInterface) {
                throw new Exception("$handlerClass should implement FormatHandlerInterface");
            }

            $this->handlersMap[$handlerInstance->getFormat()] = $handlerInstance;
        }
    }
}
