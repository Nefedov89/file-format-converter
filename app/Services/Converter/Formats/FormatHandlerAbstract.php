<?php

declare(strict_types = 1);

namespace App\Services\Converter\Formats;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use function fopen, fclose, is_dir, mkdir;

/**
 * Class FormatHandlerAbstract
 *
 * @package App\Services\Converter\Formats
 */
abstract class FormatHandlerAbstract implements FormatHandlerInterface
{
    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return void
     *
     * @throws \Exception
     */
    public function validateFormat(UploadedFile $file): void
    {
        if (!Str::contains($file->getClientMimeType(), $this->getFormat())) {
            throw new Exception("Invalid file format, {$this->getFormat()} is expected");
        }
    }

    /**
     * Crate file and execute given callback given from each format handler to put data into the file.
     *
     * @param string $format
     * @param callable $setDataCallback
     *
     * @return string Public path to created file
     */
    public function handleFileGeneration(string $format, callable $setDataCallback): string
    {
        $conversionId = uniqid();
        $basePath = App::storagePath().'/app/public/'.$conversionId;

        if (!is_dir($basePath)) {
            mkdir($basePath);
        }

        $fileName = "countries.$format";
        $filePath = "$basePath/$fileName";
        $filePointer = fopen($filePath, 'w');

        $setDataCallback($filePointer, $filePath);

        fclose($filePointer);

        return "/storage/$conversionId/$fileName";
    }
}
