<?php

declare(strict_types = 1);

namespace App\Services\Converter\Formats;

use DOMDocument;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use function simplexml_load_file;

/**
 * Class XmlFormatHandler
 *
 * @package App\Services\Converter\Formats
 */
class XmlFormatHandler extends FormatHandlerAbstract
{
    /**
     * @return string
     */
    public function getFormat(): string
    {
        return 'xml';
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return array
     *
     * @throws \Exception
     */
    public function parseDataFromFile(UploadedFile $file): array
    {
        $this->validateFormat($file);

        $content = simplexml_load_file($file->getRealPath());
        $items = [];

        foreach ($content->element as $item) {
            $items[] = (array) $item;
        }

        return $items;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function createFileWithData(array $data): string
    {
        return $this->handleFileGeneration(
            $this->getFormat(),
            function ($filePointer, $filePath) use ($data) {
                $xml = new DOMDocument();
                $root = $xml->createElement('root');

                foreach ($data as $row) {
                    $element = $xml->createElement('element');

                    foreach ($row as $key => $value) {
                        $row = $xml->createElement($key);
                        $node = $xml->createTextNode($value);

                        $row->appendChild($node);
                        $element->appendChild($row);
                    }

                    $root->appendChild($element);
                }

                $xml->appendChild($root);

                $xml->save($filePath);
            }
        );
    }
}
