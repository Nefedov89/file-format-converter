<?php

declare(strict_types = 1);

namespace App\Services\Converter\Formats;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface FormatHandlerInterface
 *
 * @package App\Services\Converter\Formats
 */
interface FormatHandlerInterface
{
    /**
     * Return handle format: xml, csv, json.
     *
     * @return string
     */
    public function getFormat(): string;

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return array
     */
    public function parseDataFromFile(UploadedFile $file): array;

    /**
     * Creates a file from given data and return public path to file.
     *
     * @param array $data
     *
     * @return string
     */
    public function createFileWithData(array $data): string;
}
