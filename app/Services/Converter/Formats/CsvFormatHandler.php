<?php

declare(strict_types = 1);

namespace App\Services\Converter\Formats;

use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use const false, null;

use function fopen, fgetcsv, fclose, strtolower, fputcsv;

/**
 * Class CsvFormatHandler
 *
 * @package App\Services\Converter\Formats
 */
class CsvFormatHandler extends FormatHandlerAbstract
{
    /**
     * @return string
     */
    public function getFormat(): string
    {
        return 'csv';
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return array
     *
     * @throws \Exception
     */
    public function parseDataFromFile(UploadedFile $file): array
    {
        $this->validateFormat($file);

        $items = [];
        $row = 1;
        $countryKey = null;
        $capitalKey = null;

        if (($handle = fopen($file->getRealPath(), 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($row === 1) {
                    $countryKey = strtolower(Arr::first($data));
                    $capitalKey = strtolower(Arr::last($data));
                } else {
                    $items[] = [
                        $countryKey => Arr::first($data),
                        $capitalKey => Arr::last($data),
                    ];
                }

                $row++;
            }

            fclose($handle);
        }

        return $items;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function createFileWithData(array $data): string
    {
        return $this->handleFileGeneration(
            $this->getFormat(),
            function ($filePointer, $filePath) use ($data) {
                $list = [['Country', 'Capital']];

                foreach ($data as $item) {
                    $list[] = [Arr::first($item), Arr::last($item)];
                }

                foreach ($list as $fields) {
                    fputcsv($filePointer, $fields);
                }
            }
        );
    }
}
