<?php

declare(strict_types = 1);

namespace App\Services\Converter\Formats;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use const true;

use function json_decode, file_get_contents, fwrite, json_encode;

/**
 * Class JsonFormatHandler
 *
 * @package App\Services\Converter\Formats
 */
class JsonFormatHandler extends FormatHandlerAbstract
{
    /**
     * @return string
     */
    public function getFormat(): string
    {
        return 'json';
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return array
     *
     * @throws \Exception
     */
    public function parseDataFromFile(UploadedFile $file): array
    {
        $this->validateFormat($file);

        $content = json_decode(file_get_contents($file->getRealPath()), true);
        $items = [];

        foreach ($content as $item) {
            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function createFileWithData(array $data): string
    {
        return $this->handleFileGeneration(
            $this->getFormat(),
            function ($filePointer, $filePath) use ($data) {
                fwrite($filePointer, json_encode($data));
            }
        );
    }
}
