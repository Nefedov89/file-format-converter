<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Requests\Converter\DownloadNewFormatRequest;
use App\Http\Requests\Converter\InputFileRequest;
use App\Services\Converter\ConverterHandler;
use Exception;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ConverterController
 *
 * @package App\Http\Controllers
 */
class ConverterController extends Controller
{
    /**
     * @param \App\Http\Requests\Converter\InputFileRequest $request
     * @param \App\Services\Converter\ConverterHandler $converter
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function parseFile(InputFileRequest $request, ConverterHandler $converter): JsonResponse
    {
        $file = $request->file('file');
        $format = $file->getClientOriginalExtension();
        $formatHandler = $converter->getFormatHandler($format);

        if (!$formatHandler) {
            throw new Exception("Can not find a handler for this format: $format");
        }

        return JsonResponse::create(
            $formatHandler->parseDataFromFile($file),
            Response::HTTP_OK
        );
    }

    /**
     * @param \App\Http\Requests\Converter\DownloadNewFormatRequest $request
     * @param \App\Services\Converter\ConverterHandler $converter
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function downloadNewFormat(
        DownloadNewFormatRequest $request,
        ConverterHandler $converter
    ): JsonResponse {
        $format = $request->get('format');
        $formatHandler = $converter->getFormatHandler($format);

        if (!$formatHandler) {
            throw new Exception("Can not find a handler for this format: $format");
        }

        return JsonResponse::create([
            'filePath' => $formatHandler->createFileWithData($request->get('items')),
        ], Response::HTTP_OK);
    }
}
