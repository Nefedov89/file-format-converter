<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Services\Converter\ConverterHandler;
use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Support\Facades\View;

/**
 * Class MainController
 *
 * @package App\Http\Controllers
 */
class MainController extends Controller
{
    /**
     * @param \App\Services\Converter\ConverterHandler $converter
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(ConverterHandler $converter): ViewContract
    {
        return View::make(
            'main',
            [
                'formats' => $converter->getAvailableFormats(),
            ]
        );
    }
}
