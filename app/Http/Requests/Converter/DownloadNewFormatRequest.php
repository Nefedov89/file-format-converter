<?php

declare(strict_types = 1);

namespace App\Http\Requests\Converter;

use App\Services\Converter\ConverterHandler;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

use function implode;

/**
 * Class DownloadNewFormatRequest
 *
 * @package App\Http\Requests\Converter
 */
class DownloadNewFormatRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'format' => [
                'required',
                'in:'.implode(
                    ',',
                    App::make(ConverterHandler::class)->getAvailableFormats()
                ),
            ],
            'items' => [
                'required',
                'array',
            ],
            'items.*' => [
                'required',
                'array',
            ],
            'items.*.country' => [
                'required',
                'string',
            ],
            'items.*.capital' => [
                'required',
                'string',
            ],
        ];
    }
}
