<?php

declare(strict_types = 1);

namespace App\Http\Requests\Converter;

use App\Services\Converter\ConverterHandler;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

use function implode;

/**
 * Class InputFileRequest
 *
 * @package App\Http\Requests
 */
class InputFileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                'file',
                'is_file_of_extension:'.implode(
                    ',',
                    App::make(ConverterHandler::class)->getAvailableFormats()
                ),
            ],
        ];
    }
}
