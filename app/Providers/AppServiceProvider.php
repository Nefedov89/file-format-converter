<?php

declare(strict_types = 1);

namespace App\Providers;

use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use App\Services\Converter\ConverterHandler;

use function in_array, implode, str_replace;

/**
 * Class AppServiceProvider
 *
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(ConverterHandler::class, function () {
            return new ConverterHandler();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->extendValidator();
    }

    /**
     * @return void
     */
    protected function extendValidator(): void
    {
        Validator::extend(
            'is_file_of_extension',
            function ($attribute, $value, $parameters, $validator) {
                return $value instanceof File
                    && in_array($value->getClientOriginalExtension(), $parameters);
            }
        );

        Validator::replacer(
            'is_file_of_extension',
            function ($message, $attribute, $rule, $parameters) {
                return str_replace(':parameters', implode(',', $parameters), $message);
            }
        );
    }
}
