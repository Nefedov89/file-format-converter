<?php

declare(strict_types = 1);

namespace App\Console\Commands;

use App\Services\Converter\ConverterHandler;
use Illuminate\Console\Command;
use Symfony\Component\HttpFoundation\File\UploadedFile as File;

use function in_array, file_exists, implode, pathinfo;

/**
 * Class ConvertCountriesCommand
 *
 * @package App\Console\Commands
 */
class ConvertCountriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:countries {input-file} {output-format}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert country files to different formats';

    /**
     * Execute the console command.
     *
     * @param \App\Services\Converter\ConverterHandler $converter
     *
     * @return void
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function handle(ConverterHandler $converter): void
    {
        $expectedFormat = $this->argument('output-format');
        $availableFormats = $converter->getAvailableFormats();
        $filePath = $this->argument('input-file');

        if (!file_exists($filePath)) {
            $this->alert("File $filePath doesn't exist");

            return;
        }

        if (!in_array($expectedFormat, $availableFormats)) {
            $this->alert(
                "Unsupported format $expectedFormat. Currently only these formats are supported: "
                .implode(', ', $availableFormats)
            );

            return;
        }

        $fileInfo = pathinfo($filePath);
        $inputFileHandler = $converter->getFormatHandler($fileInfo['extension']);
        $file = new File(
            $filePath,
            'countries.'.$fileInfo['extension'],
            $converter->getMimeType($fileInfo['extension'])
        );
        $parsedData = $inputFileHandler->parseDataFromFile($file);

        $this->info('Data from file');
        $this->table([], $parsedData);

        $outputFileHandler = $converter->getFormatHandler($expectedFormat);
        $convertedFilePublicPath = $outputFileHandler->createFileWithData($parsedData);
        $convertedFileRealPath = $converter->getGeneratedFileRealPath($convertedFilePublicPath);

        $this->info("File has been successfully created and it is located here: $convertedFileRealPath");
    }
}
