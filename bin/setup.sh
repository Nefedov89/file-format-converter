#!/usr/bin/env bash

php artisan key:generate
php artisan migrate --force
php artisan config:clear
php artisan cache:clear
php artisan view:cache
php artisan ziggy:generate 'resources/js/generated/routes.js'
php artisan vue-i18n:generate
php artisan storage:link

npm install && npm run prod

