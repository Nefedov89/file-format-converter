<?php

declare(strict_types = 1);

return [
    'gitlab_link' => 'Gitlab link of this project',
];
