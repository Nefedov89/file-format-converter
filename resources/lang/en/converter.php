<?php

declare(strict_types = 1);

return [
    'title' => 'Formats converter',
    'form' => [
        'input_file' => [
            'placeholder' => 'Choose a file you want to convert from',
            'tip' => 'Only next formats are allowed: :formats',
        ],
        'uploaded_items' => [
            'label' => 'There are items from your uploaded file',
            'tip' => 'You can add new item, delete or edit existing ones',
            'add' => 'Add new one',
            'delete' => 'Delete',
            'country' => 'Country',
            'capital' => 'Capital',
        ],
        'convert' => 'Convert',
        'download_converted_file' => 'Download converted file',
        'new_format' => [
            'label' => 'Choose new format from converting',
        ],
    ],
    'messages' => [
        'success' => 'File has been successfully converted! Use link below to download converted file',
    ],
];
