<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{!! csrf_token() !!}">

        <title>{{ Config::get('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">
        <link rel="icon" href="https://getbootstrap.com/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
        <link rel="icon" href="https://getbootstrap.com/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    </head>
    <body>
        <div id="app">
            <div class="container">
                <div class="py-5 text-center">
                    <h2>{{ Config::get('app.name') }}</h2>
                    <div class="text-center">
                        <a href="https://gitlab.com/Nefedov89/file-format-converter" target="_blank">
                            @lang('general.gitlab_link')
                        </a>
                    </div>
                </div>
                <converter-form :formats="{{ json_encode($formats) }}"></converter-form>
            </div>
        </div>

        <script src="{{ URL::asset('js/app.js') }}"></script>
    </body>
</html>
