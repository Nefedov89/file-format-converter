require('./bootstrap');

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import extendVue from './extensions/vue';
import i18n from './utils/i18n';
import components from './components';

window.Vue = require('vue');

extendVue(Vue);


// Set external plugins.
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

const app = new Vue({
    el: '#app',
    i18n,
    components,
});
