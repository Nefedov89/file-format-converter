import route from 'ziggy';
import { Ziggy } from '../generated/routes';

export default function extend(Vue) {
    Vue.mixin({
        methods: {
            route: (name, params, absolute) => route(name, params, absolute, Ziggy),
        }
    });
}
