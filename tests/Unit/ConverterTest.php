<?php

declare(strict_types = 1);

namespace Tests\Unit;

use App\Services\Converter\ConverterHandler;
use App\Services\Converter\Formats\FormatHandlerInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile as File;

use function in_array, file_exists, pathinfo;

/**
 * Class ConverterTest
 *
 * @package Tests\Unit
 */
class ConverterTest extends TestCase
{
    /**
     * @var \App\Services\Converter\ConverterHandler
     */
    private $converter;

    /**
     * @var array
     */
    private $defaultItemsSet;

    /**
     * @var array
     */
    private $modifiedItemsSets;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->converter = App::make(ConverterHandler::class);
        $this->defaultItemsSet = [
            [
                'country' => 'Ukraine',
                'capital' => 'Kyiv',
            ],
            [
                'country' => 'Germany',
                'capital' => 'Berlin',
            ],
            [
                'country' => 'USA',
                'capital' => 'Washington',
            ],
        ];
        $this->modifiedItemsSets = [
            'add' => [
                [
                    'country' => 'Ukraine',
                    'capital' => 'Kyiv',
                ],
                [
                    'country' => 'Germany',
                    'capital' => 'Berlin',
                ],
                [
                    'country' => 'USA',
                    'capital' => 'Washington',
                ],
                [
                    'country' => 'France',
                    'capital' => 'Paris',
                ],
            ],
            'delete' => [
                [
                    'country' => 'Ukraine',
                    'capital' => 'Kyiv',
                ],
                [
                    'country' => 'Germany',
                    'capital' => 'Berlin',
                ],
            ],
            'change' => [
                [
                    'country' => 'China',
                    'capital' => 'Beijing',
                ],
                [
                    'country' => 'Belgium',
                    'capital' => 'Brussels',
                ],
            ],
        ];
        $this->fileName = 'countries';
    }

    /**
     * @return void
     */
    public function testAvailableFormatHandlers(): void
    {
        foreach ($this->converter->getAvailableFormats() as $format) {
            $this->assertTrue(
                $this->converter->getFormatHandler($format) instanceof FormatHandlerInterface
            );
        }
    }

    /**
     * @return void
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function testParsedData(): void
    {
        foreach ($this->converter->getAvailableFormats() as $format) {
            $file = new File(
                $this->getFilePath($format),
                "$this->fileName.$format",
                $this->converter->getMimeType($format)
            );
            $handler = $this->converter->getFormatHandler($format);

            $this->assertEquals(
                $handler->parseDataFromFile($file),
                $this->defaultItemsSet
            );
        }
    }

    /**
     * @return void
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function testConvertedFileGeneration(): void
    {
        foreach ($this->converter->getAvailableFormats() as $formatConvertTo) {
            $handler = $this->converter->getFormatHandler($formatConvertTo);

            foreach (['add', 'delete', 'change'] as $operation) {
                $parsedData = $this->modifiedItemsSets[$operation];

                $filePublicPath = $handler->createFileWithData($parsedData);
                $fileRealPath = $this->converter->getGeneratedFileRealPath($filePublicPath);
                $fileInfo = pathinfo($fileRealPath);

                // Check that file exists.
                $this->assertTrue(file_exists($fileRealPath));

                // Check whether format is correct.
                $this->assertEquals($fileInfo['extension'], $formatConvertTo);

                // Check whether data in new file is equal to input modified data.
                $newCreatedFile = new File(
                    $fileRealPath,
                    "$this->fileName.$formatConvertTo",
                    $this->converter->getMimeType($formatConvertTo)
                );

                $this->assertEquals(
                    $parsedData,
                    $handler->parseDataFromFile($newCreatedFile)
                );
            }
        }
    }

    /**
     * @param string $format
     *
     * @return string
     */
    private function getFilePath(string $format): string
    {
        return in_array($format, $this->converter->getAvailableFormats())
            ? App::storagePath()."/app/data/$this->fileName.$format"
            : '';
    }
}
