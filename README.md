## Set up project locally

1. run in console `composer install`
2. run in console `cp .env.example .env`
3. in .env file set your local variables
4. run tests `php artisan test`
5. run converter console command `php artisan convert:countries {input-file} {output-format}` (php artisan convert:countries /path/to/file/countries.csv xml)
