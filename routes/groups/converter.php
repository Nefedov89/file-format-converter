<?php

declare(strict_types = 1);

Route::post('parse-file', [
    'as'   => '.parse-file',
    'uses' => 'ConverterController@parseFile',
]);

Route::post('download-new-format', [
    'as'   => '.download-new-format',
    'uses' => 'ConverterController@downloadNewFormat',
]);
